const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const userDao = require('../models/dao/userDao');

const register = async (req, res) => {
  const {email, password, role} = req.body;
  const checkUser = await User.findOne({email});

  if (checkUser) {
    return res.status(400).json({
      message: `User with the email "${email}" already exists`,
    });
  }

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();

  res.json({message: 'Profile created successfully'});
};

const login = async (req, res) => {
  const {email, password} = req.body;

  const user = await userDao.getUserByEmail(email);

  if ( !(await bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({message: `Wrong password`});
  }

  const token = jwt.sign({
    email: user.email,
    _id: user._id,
    createdDate: user.createdDate,
    role: user.role,
  }, JWT_SECRET);
  res.json({jwt_token: token});
};

module.exports = {
  register,
  login,
};
