/* eslint camelcase: ["error", {
  properties: "never",
  ignoreDestructuring: true
}]*/
const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const loadDao = require('../models/dao/loadDao');

const addLoad = async (req, res) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  const load = new Load({
    created_by: req.user._id,
    name: name,
    payload: payload,
    pickup_address: pickup_address,
    delivery_address: delivery_address,
    dimensions: dimensions,
  });

  await load.save();

  res.json({message: `Load created successfully`});
};

const getLoads = async (req, res) => {
  let loads;
  const {offset = 0, limit = 10} = req.query;

  if (req.user.role === 'SHIPPER') {
    loads = await Load.find(
        {},
        [],
        {
          skip: parseInt(offset),
          limit: limit > 50 ? 50 : parseInt(limit),
        },
    );
  } else {
    loads = await Load.find(
        {status: {$ne: 'NEW'}},
        [],
        {
          skip: parseInt(offset),
          limit: limit > 50 ? 50 : parseInt(limit),
        },
    );
  }

  res.json({loads: loads});
};

const updateLoad = async (req, res) => {
  const {id} = req.params;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  const load = await loadDao.findLoadById(id);

  await Load.updateOne(load, {
    name: name,
    payload: payload,
    pickup_address: pickup_address,
    delivery_address: delivery_address,
    dimensions: dimensions,
  });

  res.json({message: 'Load details changed successfully'});
};

const deleteLoad = async (req, res) => {
  const {id} = req.params;
  const load = await loadDao.findLoadById(id);

  await Load.deleteOne(load);

  res.json({message: 'Load deleted successfully'});
};

const postLoad = async (req, res) => {
  const {id} = req.params;
  const load = await loadDao.findLoadById(id);
  const {width, length, height} = load.dimensions;
  const weight = load.payload;
  let dimensionPrior;
  let payloadPrior;
  const types = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

  await Load.updateOne({_id: id}, {status: 'POSTED'});

  if (weight <= 1700) {
    payloadPrior = 0;
  } else if (weight <= 2500) {
    payloadPrior = 1;
  } else if (weight <= 4000) {
    payloadPrior = 2;
  } else {
    return res.status(400).json({message: 'Appropriate weight'});
  }

  if (width <= 300 && length <= 250 && height <= 170) {
    dimensionPrior = 0;
  } else if (width <= 500 && length <= 250 && height <= 170) {
    dimensionPrior = 1;
  } else if (width <= 700 && length <= 350 && height <= 200) {
    dimensionPrior = 2;
  } else {
    return res.status(400).json({message: 'Appropriate dimensions'});
  }

  const prior = dimensionPrior >= payloadPrior ? dimensionPrior : payloadPrior;

  const truck = await Truck.findOne({
    assigned_to: {$ne: ''},
    status: 'IS',
    type: types[prior],
  });

  if (!truck) {
    await Load.updateOne(
        {_id: id},
        {
          status: 'NEW',
          $push: {
            logs: [{
              message: 'The load has not been posted',
              driver_found: false,
            }],
          },
        },
    );

    return res.status(400).json({
      message: 'The load has not been posted',
      driver_found: false,
    });
  }

  await Truck.updateOne({assigned_to: truck.assigned_to}, {status: 'OL'});
  await Load.updateOne(
      {_id: id},
      {
        status: 'ASSIGNED',
        state: 'En route to Pick Up',
        assigned_to: truck.assigned_to,
        $push: {
          logs: [{
            message: 'Load posted successfully',
            driver_found: true,
          }],
        },
      },
  );

  res.json({message: 'Load posted successfully', driver_found: true});
};

const getActiveLoad = async (req, res) => {
  const load = await Load.findOne({assigned_to: req.user._id});

  if (!load) {
    return res.status(400).json({message: 'There is no assigned load'});
  }

  res.json({load: load});
};

const patchLoadState = async (req, res) => {
  const load = await Load.findOne({assigned_to: req.user._id});
  const states = [
    'En route to Pick Up', 'Arrived to Pick Up',
    'En route to delivery', 'Arrived to delivery',
  ];

  if (!load) {
    return res.status(400).json({message: 'There is no assigned load'});
  }

  let stateIdx = states.indexOf(load.state);

  if (stateIdx >= states.length - 1) {
    return res.status(400).json({
      message: 'The load is already in the last state',
    });
  }

  stateIdx++;

  if (states[stateIdx] === 'Arrived to delivery') {
    await Load.updateOne({assigned_to: req.user._id}, {status: 'SHIPPED'});
    await Truck.updateOne({assigned_to: req.user._id}, {status: 'IS'});
  }

  await Load.updateOne({assigned_to: req.user._id}, {state: states[stateIdx]});

  res.json({message: `Load state changed to ${states[stateIdx]}`});
};

const getShippingInfo = async (req, res) => {
  const {id} = req.params;
  const load = await Load.findOne({_id: id, assigned_to: {$ne: ''}});

  if (!load) {
    return res.status(400).json({message: 'There is no appropriate load'});
  }

  const truck = await Truck.findOne({assigned_to: load.assigned_to});

  res.json({load: load, truck: truck});
};

module.exports = {
  addLoad,
  getLoads,
  updateLoad,
  deleteLoad,
  postLoad,
  getActiveLoad,
  patchLoadState,
  getShippingInfo,
};
