const {Truck} = require('../models/truckModel');
const truckDao = require('../models/dao/truckDao');

const addTruck = async (req, res) => {
  const {type} = req.body;
  const truck = new Truck({type: type, created_by: req.user._id});

  await truck.save();

  res.json({message: 'Truck created successfully'});
};

const getTrucks = async (req, res) => {
  const trucks = await Truck.find({created_by: req.user._id});

  res.json({trucks: trucks});
};

const getTruckById = async (req, res) => {
  const {id} = req.params;
  const truck = await truckDao.findTruckById(id, req.user._id);

  res.json({truck: truck});
};

const assignTruck = async (req, res) => {
  const {id} = req.params;
  const truck = await truckDao.findTruckById(id, req.user._id);

  await Truck.updateOne(truck, {assigned_to: req.user._id});

  res.json({message: `Truck assigned successfully`});
};

const updateTruck = async (req, res) => {
  const {id} = req.params;
  const truck = await truckDao.findTruckById(id, req.user._id);
  const {type} = req.body;

  if (truck.assigned_to) {
    return res.status(400).json({
      message: `You can manage only not assigned trucks`,
    });
  }

  await Truck.updateOne(truck, {type: type});

  res.json({message: `Truck details changed successfully`});
};

const deleteTruck = async (req, res) => {
  const {id} = req.params;
  const truck = await truckDao.findTruckById(id, req.user._id);

  if (truck.assigned_to) {
    return res.status(400).json({
      message: `You can manage only not assigned trucks`,
    });
  }

  await Truck.deleteOne(truck);

  res.json({message: `Truck deleted successfully`});
};

module.exports = {
  addTruck,
  getTrucks,
  getTruckById,
  assignTruck,
  updateTruck,
  deleteTruck,
};
