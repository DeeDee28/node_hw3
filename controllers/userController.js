const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

const getMyProfile = async (req, res) => {
  const user = req.user;

  res.json({
    'user': {
      '_id': user._id,
      'email': user.email,
      'created_date': user.createdDate,
    },
  });
};

const deleteMyProfile = async (req, res) => {
  const user = req.user;

  await User.deleteOne({email: user.email});

  res.json({message: 'Profile deleted successfully'});
};

const patchMyProfile = async (req, res) => {
  const user = req.user;
  const {oldPassword, newPassword} = req.body;

  const oldUser = await User.findOne({username: user.username});

  if ( !(await bcrypt.compare(oldPassword, oldUser.password)) ) {
    return res.status(400).json({message: `Wrong old password`});
  }

  await User.updateOne(
      {email: user.email}, {password: await bcrypt.hash(newPassword, 10)},
  );

  res.json({message: 'Password changed successfully'});
};

module.exports = {
  getMyProfile,
  deleteMyProfile,
  patchMyProfile,
};
