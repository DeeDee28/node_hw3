const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadsRouter = require('./routers/loadsRouter');
const {ClientError} = require('./routers/helpers');

app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadsRouter);

app.use((err, req, res, next) => {
  if (err instanceof ClientError) {
    return res.status(400).json({message: err.message});
  }

  res.status(500).json({message: err.message});
});

app.use('*', (req, res) => {
  res.status(404).json({message: 'Not Found'});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://DeeDee28:ihOnb176oYzcE5x8@cluster0.zgkmb.mongodb.net/hw3?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server has started on port: ${port}`);
  });
};

start();
