const {Load} = require('../loadModel');
const {ClientError} = require('../../routers/helpers');

const findLoadById = async (id) => {
  const load = await Load.findOne({_id: id});

  if (!load) {
    throw new ClientError('There is no load with such an id');
  }

  if (load.status !== 'NEW') {
    throw new ClientError('You can manage only a new load');
  }

  return load;
};

module.exports = {
  findLoadById,
};
