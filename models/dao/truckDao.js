const {Truck} = require('../truckModel');
const {ClientError} = require('../../routers/helpers');

const findTruckById = async (id, userId) => {
  const truck = await Truck.findOne({_id: id});

  if (!truck) {
    throw new ClientError(`There is no truck with such an id`);
  }

  if (truck.created_by !== userId) {
    throw new ClientError(`This isn't your truck`);
  }

  return truck;
};

module.exports = {
  findTruckById,
};
