const {User} = require('../userModel');
const {ClientError} = require('../../routers/helpers');

const getUserByEmail = async (email) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new ClientError(`There is no user with the email: ${email}`);
  }

  return user;
};

module.exports = {
  getUserByEmail,
};
