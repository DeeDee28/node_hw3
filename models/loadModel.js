/* eslint new-cap: ["error", { "capIsNewExceptionPattern": "^mongoose\.." }] */
const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  status: {
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
    default: 'En route to Pick Up',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: Array,
    default: [{
      message: 'Load assigned to driver with id ###',
      time: new Date(Date.now()),
    }],
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Load = mongoose.model('Load', loadSchema);
