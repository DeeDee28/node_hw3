/* eslint new-cap: ["error", { "capIsNewExceptionPattern": "^mongoose\.." }] */
const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.User = mongoose.model('User', userSchema);
