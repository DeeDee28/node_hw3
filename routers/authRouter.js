/* eslint new-cap: ["error", { "capIsNewExceptionPattern": "^express\.." }] */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');
const {register, login} = require('../controllers/authController');
const {
  validateRegister,
  validateLogin,
} = require('./middlewares/validation');

router.post(
    '/register',
    asyncWrapper(validateRegister),
    asyncWrapper(register),
);
router.post('/login', asyncWrapper(validateLogin), asyncWrapper(login));

module.exports = router;
