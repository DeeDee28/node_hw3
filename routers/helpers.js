/* eslint "require-jsdoc": [2, {
    "require": {
        "ClassDeclaration": false
    }
}]*/

const asyncWrapper = (callback) => {
  return (req, res, next) => {
    callback(req, res, next)
        .catch(next);
  };
};

class ClientError extends Error {
  constructor(message) {
    super(message);
  }
}

module.exports = {
  ClientError,
  asyncWrapper,
};
