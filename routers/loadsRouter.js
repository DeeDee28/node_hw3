/* eslint new-cap: ["error", { "capIsNewExceptionPattern": "^express\.." }] */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');

const {
  authMiddleware,
} = require('../routers/middlewares/authMiddleware');

const {
  isShipper, isDriver,
} = require('../routers/middlewares/roleMiddleware');

const {validateUpdateLoad} = require('./middlewares/validation');

const {
  addLoad,
  getLoads,
  updateLoad,
  deleteLoad,
  postLoad,
  getActiveLoad,
  patchLoadState,
  getShippingInfo,
} = require('../controllers/loadController');

router.get(
    '/',
    authMiddleware,
    asyncWrapper(getLoads),
);
router.post(
    '/',
    authMiddleware,
    isShipper,
    asyncWrapper(addLoad),
);
router.delete(
    '/:id',
    authMiddleware,
    isShipper,
    asyncWrapper(deleteLoad),
);
router.post(
    '/:id/post',
    authMiddleware,
    isShipper,
    asyncWrapper(postLoad),
);
router.get(
    '/:id/shipping_info',
    authMiddleware,
    isShipper,
    asyncWrapper(getShippingInfo),
);
router.put(
    '/:id',
    authMiddleware,
    isShipper,
    asyncWrapper(validateUpdateLoad),
    asyncWrapper(updateLoad),
);
router.get(
    '/active',
    authMiddleware,
    isDriver,
    asyncWrapper(getActiveLoad),
);
router.patch(
    '/active/state',
    authMiddleware,
    isDriver,
    asyncWrapper(patchLoadState),
);


module.exports = router;
