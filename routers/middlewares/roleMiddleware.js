const isDriver = (req, res, next) => {
  if (req.user.role !== 'DRIVER') {
    return res.status(400).json({message: `You are not a driver`});
  };

  next();
};

const isShipper = (req, res, next) => {
  if (req.user.role !== 'SHIPPER') {
    return res.status(400).json({message: `You are not a shipper`});
  };

  next();
};

module.exports = {
  isDriver,
  isShipper,
};
