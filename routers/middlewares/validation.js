const Joi = require('joi');

const validateRegister = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),

    role: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};

const validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .required(),

    password: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};

const validateChangePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .required(),

    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await schema.validateAsync(req.body);
  next();
};

const validateType = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};

const validateUpdateLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string()
        .required(),
    payload: Joi.number()
        .required(),
    pickup_address: Joi.string()
        .required(),
    delivery_address: Joi.string()
        .required(),
    dimensions: Joi.object({
      width: Joi.number().required(),
      length: Joi.number().required(),
      height: Joi.number().required(),
    }),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports = {
  validateRegister,
  validateLogin,
  validateChangePassword,
  validateType,
  validateUpdateLoad,
};
