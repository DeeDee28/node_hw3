/* eslint new-cap: ["error", { "capIsNewExceptionPattern": "^express\.." }] */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');

const {
  authMiddleware,
} = require('../routers/middlewares/authMiddleware');

const {
  isDriver,
} = require('../routers/middlewares/roleMiddleware');

const {
  addTruck,
  getTrucks,
  getTruckById,
  assignTruck,
  updateTruck,
  deleteTruck,
} = require('../controllers/truckController');

const {validateType} = require('./middlewares/validation');

router.get(
    '/',
    authMiddleware,
    isDriver,
    asyncWrapper(getTrucks),
);
router.post(
    '/',
    authMiddleware,
    isDriver,
    asyncWrapper(validateType),
    asyncWrapper(addTruck),
);
router.get(
    '/:id',
    authMiddleware,
    isDriver,
    asyncWrapper(getTruckById),
);
router.put(
    '/:id',
    authMiddleware,
    isDriver,
    asyncWrapper(validateType),
    asyncWrapper(updateTruck),
);
router.delete(
    '/:id',
    authMiddleware,
    isDriver,
    asyncWrapper(deleteTruck),
);
router.post(
    '/:id/assign',
    authMiddleware,
    isDriver,
    asyncWrapper(assignTruck),
);

module.exports = router;
