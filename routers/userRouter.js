/* eslint new-cap: ["error", { "capIsNewExceptionPattern": "^express\.." }] */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');

const {
  getMyProfile,
  patchMyProfile,
  deleteMyProfile,
} = require('../controllers/userController');

const {validateChangePassword} = require('./middlewares/validation');

const {
  authMiddleware,
} = require('../routers/middlewares/authMiddleware');

const {
  isShipper,
} = require('../routers/middlewares/roleMiddleware');

router.get(
    '/',
    authMiddleware,
    asyncWrapper(getMyProfile),
);
router.patch(
    '/password',
    authMiddleware,
    asyncWrapper(validateChangePassword),
    asyncWrapper(patchMyProfile),
);
router.delete(
    '/',
    authMiddleware,
    isShipper,
    asyncWrapper(deleteMyProfile),
);

module.exports = router;
